
class PuzzleGame {
    // Each number represents a card. And 0 represents the empty cell.
    // Actual cards are aligned in 2-dimensional space, but internally used 1-dimentional array.
    cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];

    constructor() {
        // Align cards in random order
        this.shuffle();
    }

    shiftCard(index) {
        // If left card is empty
        if (this.cards[index - 1] === 0) {
            this.swapCards(index, index - 1);
        // If right card is empty
        } else if (this.cards[index + 1] === 0) {
            this.swapCards(index, index + 1);
        // If top card is empty
        } else if (this.cards[index - 4] === 0) {
            this.swapCards(index, index - 4);
        // If bottom card is empty
        } else if (this.cards[index + 4] === 0) {
            this.swapCards(index, index + 4);
        }
    }

    // Method to align the cards in random order.
    shuffle() {
        // In order to align the cards in random order, select random 2 card and swap their position.
        // Repeat this action at most 100 times.
        for (let i = 0; i < 100; i++) {
            const index1 = Math.floor(Math.random() * 16);
            const index2 = Math.floor(Math.random() * 16);

            if (index1 !== index2) {
                this.swapCards(index1, index2);
            }
        }
    }

    // Swap 2 cards.
    swapCards(i, j) {
        const temp = this.cards[i];
        this.cards[i] = this.cards[j];
        this.cards[j] = temp;
    }

    // Check if all cards are put in correct position.
    isFinished() {
        for (let i = 0; i < 15; i++) {
            // If any of the card is not in the correct position, return false.
            if (this.cards[i] !== i + 1) {
                return false;
            }
        }

        return true;
    }
}

export default PuzzleGame;