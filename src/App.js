/* eslint-disable no-restricted-globals */

import React, { useState } from 'react';
import PuzzleGame from './PuzzleGame';
import './App.css';

const puzzleGame = new PuzzleGame();

function App() {
  const [state, setState] = useState({
    cards: puzzleGame.cards,
  });

  const handleClickCard = (index) => {
    puzzleGame.shiftCard(index);

    setState((prevState) => ({
      ...prevState,
      cards: puzzleGame.cards,
    }));

    if (puzzleGame.isFinished()) {
      if(confirm('Congrats, You did it! Play again?')){
        puzzleGame.shuffle();
      }
    }
  }

  const handleClickShuffle = () => {
    puzzleGame.shuffle();

    setState((prevState) => ({
      ...prevState,
      cards: puzzleGame.cards,
    }));
  }

  return (
    <div className="App">
      <div className="cards">
        {
          state.cards.map((card, index) => (
            <div 
              key={index}
              className={`card ${card === 0 && 'empty'}`} 
              onClick={() => handleClickCard(index)}
            >
              {
                card !== 0 && card
              }
            </div>
          ))
        }
      </div>

      <button onClick={handleClickShuffle}>Shuffle</button>
    </div>
  );
}

export default App;
