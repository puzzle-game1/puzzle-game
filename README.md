# Simple deck of cards homework assignment.

## How to run the project.

Run `npm install` inside the root directory and then run `npm start`.
You'll see a 15 card puzzle game.


## Structure.

PuzzleGame.js is the class which represents the "deck of cards".
App.js is a simple UI file, which uses the PuzzleGame.js
App.css file is a simple styling file which is used by App.js


## Misc.

This project is developed using React.js, specifically based on create-react-app boilerplate.
